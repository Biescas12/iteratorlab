package llPositionalList;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PLElementsBackwardIterator<E> extends LinkedPositionalList<E> 
											implements Iterator<E> {
	private PLIteratorL2F<E> posIterator;
	
	public PLElementsBackwardIterator(LinkedPositionalList<E> list) {
		posIterator = new PLIteratorL2F<E>(list);
	}
	
	@Override
	public boolean hasNext() {
		return posIterator.hasNext();
	}

	@Override
	public E next() throws NoSuchElementException {
		if(!hasNext()) {
			throw new NoSuchElementException("No more elements");
		}
		return posIterator.next().getElement();
	}
	
	@Override
	public void remove() {
		posIterator.remove();
	}

}
